#!/usr/bin/env python 
# -*- coding: utf-8 -*-

class SourceError(Exception):
	pass

class ConfigError(Exception):
	pass

class NotEnoughMemoryError(Exception):
	pass

class RestoreError(Exception):
	pass

class NotFoundError(Exception):
	pass

class NotAccessError(Exception):
	pass

class SymlinkError(Exception):
	pass