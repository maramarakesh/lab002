from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='myrm',
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    entry_points={
        'console_scripts':
            ['rmv = myrm.rm_manager:Main',
             'rs = myrm.trash_manager:Main',
             'rmlogs = myrm.log:Main',
             'rmconf = myrm.config:Main']
    }
)
